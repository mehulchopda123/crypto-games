// import React from "react";
// import { Box, Center, Heading } from "native-base";

// const Home = () => {
//   return (
//     <Center flex={1}>
//       This is a sample screen. You can find more screens by swiping right on the
//       screen.
//     </Center>
//   );
// };

// export default Home;

import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Alert} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
// import firebase from 'firebase/app';
// import 'firebase/auth';
// import {loggingOut} from '../../api/firebaseMethods';

export default function Home() {

  // const handlePress = () => {
  //   loggingOut();
  // };
  const [user, setUser] = useState("");

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Dashboard</Text>
      <Text style={styles.text}>Hi {{user}}</Text>
      <TouchableOpacity style={styles.button} onPress={Alert('log out')}>
        <Text style={styles.buttonText}>Log Out</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e93b81',
    paddingTop: 50,
    paddingHorizontal: 12
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 24
  },
  titleText: {
    fontSize: 24,
    fontWeight: '600',
    color: '#fff'
  },
  text: {
    fontSize: 16,
    fontWeight: 'normal',
    color: '#fff'
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: '#fff'
  },
  button:{
    backgroundColor:'red',
    borderRadius:10
  }  
});