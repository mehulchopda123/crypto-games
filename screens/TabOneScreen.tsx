import { StyleSheet, Alert } from 'react-native';
// import  { useEffect, useState } from 'react';
// import { InterstitialAd, AdEventType, TestIds } from '@react-native-firebase/admob';
import  { useEffect, useState } from 'react';
import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import auth from '@react-native-firebase/auth';
import { Button, Box, Center, NativeBaseProvider } from "native-base";
import { 
  InterstitialAd, 
  AdEventType, TestIds,
   RewardedAd, 
   RewardedAdEventType ,
   BannerAdSize,
   BannerAd
  } from 'react-native-google-mobile-ads';




export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  const user = auth().currentUser.email
// Set global test device ID


const adUnitId = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';
const adUnitIdRewarded = __DEV__ ? TestIds.REWARDED : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';
const adUnitIdBanner = __DEV__ ? TestIds.BANNER : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';

const interstitial = InterstitialAd.createForAdRequest(adUnitId, {
  requestNonPersonalizedAdsOnly: true,
  keywords: ['fashion', 'clothing'],
});

const rewarded = RewardedAd.createForAdRequest(adUnitIdRewarded, {
  requestNonPersonalizedAdsOnly: true,
  keywords: ['fashion', 'clothing'],
});

const [loaded, setLoaded] = useState(false);
const [loadedReward, setLoadedReward] = useState(false);


useEffect(() => {
  const eventListener = interstitial.onAdEvent(type => {
    if (type === AdEventType.LOADED) {
      setLoaded(true);
    }
  });

  // Start loading the interstitial straight away
  interstitial.load();

  // Unsubscribe from events on unmount
  return () => {
    eventListener();
  };
}, []);  
  

useEffect(() => {
  const eventListener = rewarded.onAdEvent((type, error, reward) => {
    if (type === RewardedAdEventType.LOADED) {
      setLoadedReward(true);
    }

    if (type === RewardedAdEventType.EARNED_REWARD) {
      console.log('User earned reward of ', reward);
    }
  });

  // Start loading the rewarded ad straight away
  rewarded.load();

  // Unsubscribe from events on unmount
  return () => {
    eventListener();
  };
}, []);

// No advert ready to show yet
if (!loaded || !loadedReward) {
  return null;
}

  return (
    <View style={styles.container}>
       <BannerAd
          unitId={adUnitIdBanner}
          size={BannerAdSize.ADAPTIVE_BANNER}
          requestOptions={{
            requestNonPersonalizedAdsOnly: true,
          }}
        />
      <Text style={styles.title}>Welcome, {user}</Text>
     <Box alignItems="center">
      <Button onPress={() => interstitial.show()}>Show Interstitial</Button>
      <Button onPress={() => rewarded.show()}>Show Rewarded</Button>
     
    </Box>
    <View>
      {/* <BannerAd size={BannerAdSize.BANNER} unitId={TestIds.BANNER} /> */}
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
