import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import {Alert} from "react-native";


export async function registration(email, password, lastName, firstName) {
  
    await auth()
    .createUserWithEmailAndPassword(email, password)
    .then(() => {
      console.log('User account created & signed in!');
      // const subscriber = firestore()
      // .collection('Users')
      // .onSnapshot(querySnapshot => {
      //   const users = [];
      //   querySnapshot.forEach(documentSnapshot => {
      //     users.push({
      //       ...documentSnapshot.data(),
      //       name: 'ram jane',
      //     });
      //   });

    })

  // Unsubscribe from events when no longer in use
    // return () => subscriber();
  //  })
    .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
      }
  
      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
      }
  
      console.error(error);
    });

  //   await auth().createUserWithEmailAndPassword(email, password);
  //   // const currentUser = auth().currentUser;

  //   // const db = firebase.firestore();
  //   // db.collection("users")
  //   //   .doc(currentUser.uid)
  //   //   .set({
  //   //     email: currentUser.email,
  //   //     lastName: lastName,
  //   //     firstName: firstName,
  //   //   });
  // } catch (err) {
  //   Alert.alert("There is something wrong!!!!", err.message);
  // }
}

export async function signIn(email, password) {
  await auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => {
      console.log('User signed in!');
    })
    .catch(error => {
      if (error.code === 'auth/wrong-password') {
        console.log('You have provided wrong password!');
      }
  
      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
      }
  
      console.error(error);
    });  
}

export async function loggingOut() {
  try {
    await auth().signOut();
  } catch (err) {
    Alert.alert('There is something wrong!', err.message);
  }
}