// import { StatusBar } from 'expo-status-bar';
// import { SafeAreaProvider } from 'react-native-safe-area-context';
// import { NativeBaseProvider, extendTheme, theme as nbTheme } from "native-base";

// import useCachedResources from './hooks/useCachedResources';
// import useColorScheme from './hooks/useColorScheme';
// import Navigation from './navigation';

// export default function App() {
//   const isLoadingComplete = useCachedResources();
//   const colorScheme = useColorScheme();

//   const theme = extendTheme({
//     colors: {
//       primary: nbTheme.colors.violet,
//     },
//   });
//   if (!isLoadingComplete) {
//     return null;
//   } else {
//     return (
//       <NativeBaseProvider theme={theme}>
//       <SafeAreaProvider>
//         <Navigation colorScheme={colorScheme} />
//         <StatusBar />
//       </SafeAreaProvider>
//       </NativeBaseProvider>

//     );
//   }
// }


import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';
import { Platform, StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, extendTheme, theme as nbTheme } from "native-base";
//import loadAssetsAsync from './src/utilities/loadAssetsAsync';
import Navigation from './navigation';
import useColorScheme from './hooks/useColorScheme';


function useSplashScreen(loadingFunction: () => void) {
  const [isLoadingCompleted, setLoadingComplete] = React.useState(false);
 

 // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {

    async function loadAsync() {
      try {
        await SplashScreen.preventAutoHideAsync();
        await loadingFunction();
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        await SplashScreen.hideAsync();
      }
    }

    loadAsync();
    
  }, []);
 

  return isLoadingCompleted;
}



// React.useEffect(() => {
//   admob()
//  .setRequestConfiguration({
//    // Update all future requests suitable for parental guidance
//    maxAdContentRating: MaxAdContentRating.PG,

//    // Indicates that you want your content treated as child-directed for purposes of COPPA.
//    tagForChildDirectedTreatment: true,

//    // Indicates that you want the ad request to be handled in a
//    // manner suitable for users under the age of consent.
//    tagForUnderAgeOfConsent: true,
//  })
//  .then(() => {
//    // Request config successfully set!
//  });
// });


const App = () => {
  const isLoadingCompleted = useSplashScreen(async () => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content', false);
    }
   // await loadAssetsAsync();
  });
  const colorScheme = useColorScheme();

  const theme = extendTheme({
    colors: {
      primary: nbTheme.colors.violet,
    },
  });
  return isLoadingCompleted ? (
    <NativeBaseProvider theme={theme}>
    <SafeAreaProvider>
      <Navigation colorScheme={colorScheme} />
      <StatusBar />
    </SafeAreaProvider>
    </NativeBaseProvider>
  ) : null;
};

export default App;
